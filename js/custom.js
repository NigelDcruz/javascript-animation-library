$.fn.isInViewport = function() {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();
	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();
	return elementBottom > viewportTop && elementTop < viewportBottom;
};
$(window).on('resize scroll', function() {
	//Get refrence to the section
	var a = $('#a');
	//Checks if the element is in viewport
	if (a.isInViewport()){
		a.addClass('fadeInLeftBig');
	}
	else{
		a.removeClass('fadeInLeftBig');
	}
});
